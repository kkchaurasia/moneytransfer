package com.moneytransfer;

import com.moneytransfer.dao.DAOFactory;
import com.moneytransfer.exception.DataNotFoundExceptionMapper;
import com.moneytransfer.exception.DatabaseExceptionMapper;
import com.moneytransfer.exception.ForbiddenExceptionMapper;
import com.moneytransfer.service.AccountServiceImpl;
import com.moneytransfer.service.MoneyTransferServiceImpl;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * Main Class
 */
public class Application {

    private static Logger log = Logger.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        // Initialize H2 database with sample data
        log.info("Initialize .....");
        DAOFactory h2DaoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);
        h2DaoFactory.populateTestData();
        log.info("Initialisation Complete....");
        // Start jetty server
        startService();
    }

    private static void startService() throws Exception {
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                AccountServiceImpl.class.getCanonicalName() + ","
                        + MoneyTransferServiceImpl.class.getCanonicalName() + "," + DataNotFoundExceptionMapper.class.getCanonicalName()
                        + "," + DatabaseExceptionMapper.class.getCanonicalName() + "," + ForbiddenExceptionMapper.class.getCanonicalName());
        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

}
