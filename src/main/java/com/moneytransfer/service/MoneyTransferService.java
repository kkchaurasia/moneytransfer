package com.moneytransfer.service;

import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;

import javax.ws.rs.PathParam;
import java.math.BigDecimal;
import java.util.List;

public interface MoneyTransferService {
    public Account deposit(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount);

    public Account withdraw(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount);

    public List<Account> transferFund(MoneyTransfer transaction);
}
