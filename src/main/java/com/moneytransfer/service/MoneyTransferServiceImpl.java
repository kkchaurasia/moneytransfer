package com.moneytransfer.service;

import com.moneytransfer.dao.DAOFactory;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Path("/v1/transfers")
@Produces(MediaType.APPLICATION_JSON)
public class MoneyTransferServiceImpl implements MoneyTransferService {

    private final DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);

    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public Account deposit(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount) {

        if (amount.compareTo(Account.zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

        daoFactory.getAccountDAO().updateAccountBalance(accountId, amount.setScale(2, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }

    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public Account withdraw(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount) {

        if (amount.compareTo(Account.zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        daoFactory.getAccountDAO().updateAccountBalance(accountId, delta.setScale(2, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }

    @POST
    public List<Account> transferFund(MoneyTransfer transaction) {

        String currency = transaction.getCurrencyCode();
        int updateCount = daoFactory.getAccountDAO().transferAccountBalance(transaction);
        List<Account> response = new ArrayList<>();
        final Account toAccount = daoFactory.getAccountDAO().getAccountById(transaction.getToAccountId());
        final Account fromAccount = daoFactory.getAccountDAO().getAccountById(transaction.getFromAccountId());
        response.add(toAccount);
        response.add(fromAccount);
        return response;

    }
}
