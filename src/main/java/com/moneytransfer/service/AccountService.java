package com.moneytransfer.service;

import com.moneytransfer.model.Account;

import javax.ws.rs.PathParam;
import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    public List<Account> getAllAccounts();

    public Account createAccount(Account account);

    public Account getAccount(@PathParam("accountId") long accountId);

    public BigDecimal getBalance(@PathParam("accountId") long accountId);
}
