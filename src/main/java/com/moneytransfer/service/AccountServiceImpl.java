package com.moneytransfer.service;

import com.moneytransfer.dao.DAOFactory;
import com.moneytransfer.model.Account;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.List;

/**
 * Account Service
 */
@Path("/v1/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountServiceImpl implements AccountService {

    private static Logger log = Logger.getLogger(AccountServiceImpl.class);
    private final DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);


    @GET
    public List<Account> getAllAccounts() {
        return daoFactory.getAccountDAO().getAllAccounts();
    }

    @POST
    public Account createAccount(Account account) {
        final long accountId = daoFactory.getAccountDAO().createAccount(account);
        Account createdAccount = new Account(accountId, account.getUserName(), account.getBalance(), account.getCurrencyCode());
        return createdAccount;
    }

    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) {
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }

    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) {
        final Account account = daoFactory.getAccountDAO().getAccountById(accountId);
        return account.getBalance();
    }

}
