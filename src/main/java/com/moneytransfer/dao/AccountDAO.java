package com.moneytransfer.dao;

import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;

import java.math.BigDecimal;
import java.util.List;


public interface AccountDAO {

    List<Account> getAllAccounts();

    Account getAccountById(long accountId);

    long createAccount(Account account);

    int updateAccountBalance(long accountId, BigDecimal deltaAmount);

    int transferAccountBalance(MoneyTransfer moneyTransfer);
}
