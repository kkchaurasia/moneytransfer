package com.moneytransfer.dao;

import com.moneytransfer.config.ConfigReader;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * InMemory data access object
 */
public class InMemoryDAOFactory extends DAOFactory {
    private static final String driver = ConfigReader.getStringProperty("h2_driver");
    private static final String connection_url = ConfigReader.getStringProperty("h2_connection_url");
    private static final String user = ConfigReader.getStringProperty("h2_user");
    private static final String password = ConfigReader.getStringProperty("h2_password");
    private static Logger log = Logger.getLogger(InMemoryDAOFactory.class);

    private final AccountDAOImpl accountDAO = new AccountDAOImpl();

    InMemoryDAOFactory() {
        // init: load driver
        DbUtils.loadDriver(driver);
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(connection_url, user, password);

    }

    public AccountDAO getAccountDAO() {
        return accountDAO;
    }

    @Override
    public void populateTestData() {
        log.info("Populating sample data ..... ");
        Connection conn = null;
        try {
            conn = InMemoryDAOFactory.getConnection();
            RunScript.execute(conn, new FileReader("src/test/resources/sampleData.sql"));
        } catch (SQLException e) {
            log.error("Error populating user data: ", e);
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            log.error("Error finding sample data", e);
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

}
