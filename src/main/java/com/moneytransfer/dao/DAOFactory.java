package com.moneytransfer.dao;

public abstract class DAOFactory {

    public static final int H2 = 1;

    public static DAOFactory getDAOFactory(int factoryCode) {

        switch (factoryCode) {
            case H2:
                return new InMemoryDAOFactory();
            default:
                // by default using H2 in memory database
                return new InMemoryDAOFactory();
        }
    }

    public abstract AccountDAO getAccountDAO();

    public abstract void populateTestData();
}
