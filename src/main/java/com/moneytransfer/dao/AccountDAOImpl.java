package com.moneytransfer.dao;

import com.moneytransfer.exception.DataNotFoundException;
import com.moneytransfer.exception.DatabaseException;
import com.moneytransfer.exception.ForbiddenException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl implements AccountDAO {

    private final static String SQL_GET_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? ";
    private final static String SQL_LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? FOR UPDATE";
    private final static String SQL_CREATE_ACC = "INSERT INTO Account (UserName, Balance, CurrencyCode) VALUES (?, ?, ?)";
    private final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ? ";
    private final static String SQL_GET_ALL_ACC = "SELECT * FROM Account";
    private static Logger log = Logger.getLogger(AccountDAOImpl.class);

    /**
     * Get all accounts.
     */
    public List<Account> getAllAccounts() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Account> allAccounts = new ArrayList<Account>();
        try {
            conn = InMemoryDAOFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_ACC);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Account acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
                        rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
                log.debug("getAllAccounts(): Get  Account " + acc);
                allAccounts.add(acc);
            }
            return allAccounts;
        } catch (SQLException e) {
            log.error("Error on getAllAccounts " + e);
            throw new DatabaseException("getAllAccounts() failed due to internal server error");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    /**
     * Get account by id
     */
    public Account getAccountById(long accountId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Account acc = null;
        try {
            conn = InMemoryDAOFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ACC_BY_ID);
            stmt.setLong(1, accountId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
                        rs.getString("CurrencyCode"));
                log.debug("Retrieve Account By Id: " + acc);
            } else {
                throw new DataNotFoundException("Account with id " + accountId + " does not exist");
            }
            return acc;
        } catch (SQLException e) {
            log.error("Error on getAccountById:  " + accountId, e);
            throw new DatabaseException("getAccountById() failed due to internal server error");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }

    }

    /**
     * Create account
     */
    public long createAccount(Account account) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = InMemoryDAOFactory.getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_ACC);
            stmt.setString(1, account.getUserName());
            stmt.setBigDecimal(2, account.getBalance());
            stmt.setString(3, account.getCurrencyCode());
            stmt.executeUpdate();
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
            return 0;
        } catch (SQLException e) {
            log.error("Error on creating new Account  " + account, e);
            throw new DatabaseException("createAccount() failed due to internal server error");
        } finally {
            DbUtils.closeQuietly(conn, stmt, generatedKeys);
        }
    }

    /**
     * Update account balance
     */
    public int updateAccountBalance(long accountId, BigDecimal deltaAmount) {

        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        int updateCount = -1;
        try {
            updateCount = updateAccountBalanceHelper(rs, conn, lockStmt, updateStmt, accountId, deltaAmount);
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            log.error("updateAccountBalance(): User Transaction Failed, rollback initiated for: " + accountId, se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                log.error("Fail to rollback transaction ", re);
                throw new DatabaseException("updateAccountBalance() failed due to internal server error");
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return updateCount;
    }


    /**
     * Transfer balance between two accounts.
     */
    public int transferAccountBalance(MoneyTransfer moneyTransfer) {
        int result = -1;
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account fromAccount = null;
        Account toAccount = null;

        try {
            conn = InMemoryDAOFactory.getConnection();
            conn.setAutoCommit(false);

            fromAccount = fetchAccountByID(conn, lockStmt, rs, moneyTransfer.getFromAccountId());
            toAccount = fetchAccountByID(conn, lockStmt, rs, moneyTransfer.getToAccountId());

            // validate on currency denomination
            Boolean isValid = validateCurrencyDenomination(fromAccount.getCurrencyCode(), toAccount.getCurrencyCode(), moneyTransfer.getCurrencyCode());
            if (!isValid) {
                throw new ForbiddenException("Fail to transfer Fund, currency denomination are different");
            }

            // check enough fund in source account
            BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(moneyTransfer.getAmount());
            if (fromAccountLeftOver.compareTo(Account.zeroAmount) < 0) {
                throw new ForbiddenException("Not enough Fund from source Account");
            }

            // proceed with update
            result = moneyTransfer(conn, updateStmt, moneyTransfer, fromAccountLeftOver, toAccount);

            // If there is no error, commit the transaction
            conn.commit();
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            log.error("transferAccountBalance(): User Transaction Failed, rollback initiated for: " + moneyTransfer,
                    se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                log.error("Fail to rollback transaction ", re);
                throw new DatabaseException("User Transaction Failed due to internal server error");
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return result;
    }

    private Account fetchAccountByID(Connection conn, PreparedStatement lockStmt, ResultSet rs, Long accountID) throws DataNotFoundException, SQLException {
        // lock the credit and debit account for writing:
        lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
        lockStmt.setLong(1, accountID);
        rs = lockStmt.executeQuery();
        Account account = null;
        if (rs.next()) {
            account = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
                    rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
            log.debug("transferAccountBalance from Account: " + account);
        } else {
            throw new DataNotFoundException("Account with id " + accountID + " does not exist");
        }
        return account;
    }

    private boolean validateCurrencyDenomination(String fromAccountCD, String toAccountCD, String moneyTransferCD) {
        // check currencyDenomination is the same for both accounts
        if (!fromAccountCD.equals(toAccountCD)) {
            return false;
        }

        // check currencyDenomination of transaction currency
        if (!fromAccountCD.equals(moneyTransferCD)) {
            return false;
        }
        return true;
    }

    private int moneyTransfer(Connection conn, PreparedStatement updateStmt, MoneyTransfer moneyTransfer, BigDecimal fromAccountLeftOver,
                              Account toAccount) throws SQLException {

        updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
        updateStmt.setBigDecimal(1, fromAccountLeftOver);
        updateStmt.setLong(2, moneyTransfer.getFromAccountId());
        updateStmt.addBatch();

        updateStmt.setBigDecimal(1, toAccount.getBalance().add(moneyTransfer.getAmount()));
        updateStmt.setLong(2, moneyTransfer.getToAccountId());
        updateStmt.addBatch();

        int[] rowsUpdated = updateStmt.executeBatch();
        int result = rowsUpdated[0] + rowsUpdated[1];
        log.debug("Number of rows updated for the transfer : " + result);
        return result;
    }

    private int updateAccountBalanceHelper(ResultSet rs, Connection conn, PreparedStatement lockStmt, PreparedStatement updateStmt, long accountId,
                                           BigDecimal deltaAmount) throws SQLException, ForbiddenException {
        Account targetAccount = null;
        int updateCount = -1;
        conn = InMemoryDAOFactory.getConnection();
        conn.setAutoCommit(false);

        //get the target account
        targetAccount = fetchAccountByID(conn, lockStmt, rs, accountId);

        // check enough fund in source account
        BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
        if (balance.compareTo(Account.zeroAmount) < 0) {
            throw new ForbiddenException("Account doesnt have sufficent money");
        }

        // update account
        updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
        updateStmt.setBigDecimal(1, balance);
        updateStmt.setLong(2, accountId);
        updateCount = updateStmt.executeUpdate();
        conn.commit();

        log.debug("New Balance after Update: " + targetAccount);
        return updateCount;
    }

}

