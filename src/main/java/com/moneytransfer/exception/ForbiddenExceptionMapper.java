package com.moneytransfer.exception;

import com.moneytransfer.model.ErrorMesaage;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    private static Logger log = Logger.getLogger(ForbiddenExceptionMapper.class);

    @Override
    public Response toResponse(ForbiddenException ex) {
        ErrorMesaage errorMesaage = new ErrorMesaage(ex.getMessage(), 403);
        return Response.status(Response.Status.FORBIDDEN).entity(errorMesaage).build();
    }
}
