package com.moneytransfer.exception;

import com.moneytransfer.model.ErrorMesaage;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DatabaseExceptionMapper implements ExceptionMapper<DatabaseException> {
    private static Logger log = Logger.getLogger(DatabaseExceptionMapper.class);

    @Override
    public Response toResponse(DatabaseException ex) {
        ErrorMesaage errorMesaage = new ErrorMesaage(ex.getMessage(), 500);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorMesaage).build();
    }
}
