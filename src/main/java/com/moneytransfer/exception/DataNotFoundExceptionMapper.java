package com.moneytransfer.exception;

import com.moneytransfer.model.ErrorMesaage;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

    private static Logger log = Logger.getLogger(DataNotFoundExceptionMapper.class);


    @Override
    public Response toResponse(DataNotFoundException ex) {
        ErrorMesaage errorMesaage = new ErrorMesaage(ex.getMessage(), 404);
        return Response.status(Response.Status.NOT_FOUND).entity(errorMesaage).build();
    }


}
