package com.moneytransfer.dao;

import com.moneytransfer.exception.DataNotFoundException;
import com.moneytransfer.exception.ForbiddenException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;
import org.apache.commons.dbutils.DbUtils;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static junit.framework.TestCase.assertTrue;

public class TestAccountDAO {

    private static final DAOFactory h2DaoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);
    private static final int THREADS_COUNT = 100;

    @BeforeClass
    public static void setup() {
        // prepare test database and test data
        h2DaoFactory.populateTestData();
    }

    @After
    public void tearDown() {

    }

    /*
        Scenario: get all account
                  return list containing 6 account
      */
    @Test
    public void testGetAllAccounts() {
        List<Account> allAccounts = h2DaoFactory.getAccountDAO().getAllAccounts();
        assertTrue(allAccounts.size() == 7);
    }

    /*
        Scenario: get by accountID
                  return account: (1, 'abey',100.00,'USD')
      */
    @Test
    public void testGetAccountById() {
        Account account = h2DaoFactory.getAccountDAO().getAccountById(1L);
        assertTrue(account.getUserName().equals("abey"));
    }

    /*
        Scenario: get non existing account
                  return account: null
      */
    @Test(expected = DataNotFoundException.class)
    public void testGetNonExistingAccById() {
        Account account = h2DaoFactory.getAccountDAO().getAccountById(100L);
        assertTrue(account == null);
    }

    /*
        Scenario: create new account
                  assert on afterCreationID
      */
    @Test
    public void testCreateAccount() {
        BigDecimal balance = new BigDecimal(10).setScale(2, RoundingMode.HALF_EVEN);
        Account a = new Account("test2", balance, "CNY");
        long aid = h2DaoFactory.getAccountDAO().createAccount(a);
        Account afterCreation = h2DaoFactory.getAccountDAO().getAccountById(aid);
        assertTrue(afterCreation.getUserName().equals("test2"));
        assertTrue(afterCreation.getCurrencyCode().equals("CNY"));
        assertTrue(afterCreation.getBalance().equals(balance));
    }


    /*
            Scenario: deposit and withdraw on account with initial account=100L
                      assert on account balance
          */
    @Test
    public void testUpdateAccountBalanceSufficientFund() {

        BigDecimal deltaDeposit = new BigDecimal(50).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal afterDeposit = new BigDecimal(250).setScale(2, RoundingMode.HALF_EVEN);
        int rowsUpdated = h2DaoFactory.getAccountDAO().updateAccountBalance(2L, deltaDeposit);
        assertTrue(rowsUpdated == 1);
        assertTrue(h2DaoFactory.getAccountDAO().getAccountById(2L).getBalance().equals(afterDeposit));

        BigDecimal deltaWithDraw = new BigDecimal(-50).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal afterWithDraw = new BigDecimal(200).setScale(2, RoundingMode.HALF_EVEN);
        int rowsUpdatedW = h2DaoFactory.getAccountDAO().updateAccountBalance(2L, deltaWithDraw);
        assertTrue(rowsUpdatedW == 1);
        assertTrue(h2DaoFactory.getAccountDAO().getAccountById(2L).getBalance().equals(afterWithDraw));

    }

    /*
            Scenario: withdraw on account having insufficient money
                      assert on return
          */
    @Test(expected = ForbiddenException.class)
    public void testUpdateAccountBalanceNotEnoughFund() {
        BigDecimal deltaWithDraw = new BigDecimal(-50000);
        int rowsUpdatedW = h2DaoFactory.getAccountDAO().updateAccountBalance(1L, deltaWithDraw);
        assertTrue(rowsUpdatedW == 0);

    }

    /*
            Scenario: withdraw on non existing account
                      assert on return
          */
    @Test(expected = DataNotFoundException.class)
    public void testUpdateNonExistingAccount() {
        BigDecimal deltaWithDraw = new BigDecimal(-50000);
        int rowsUpdatedW = h2DaoFactory.getAccountDAO().updateAccountBalance(100L, deltaWithDraw);
        assertTrue(rowsUpdatedW == 0);

    }


    @Test
    public void testAccountMultiThreadedTransfer() throws InterruptedException, WebApplicationException {
        final AccountDAO accountDAO = h2DaoFactory.getAccountDAO();
        // transfer a total of 200USD from 100USD balance in multi-threaded
        // mode, expect half of the transaction fail
        final CountDownLatch latch = new CountDownLatch(THREADS_COUNT);
        for (int i = 0; i < THREADS_COUNT; i++) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        MoneyTransfer transaction = new MoneyTransfer("USD",
                                new BigDecimal(2).setScale(2, RoundingMode.HALF_EVEN), 1L, 3L);
                        accountDAO.transferAccountBalance(transaction);
                    } catch (Exception e) {
                    } finally {
                        latch.countDown();
                    }
                }
            }).start();
        }

        latch.await();

        Account accountFrom = accountDAO.getAccountById(1);
        Account accountTo = accountDAO.getAccountById(3);

        assertTrue(accountFrom.getBalance().equals(new BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN)));
        assertTrue(accountTo.getBalance().equals(new BigDecimal(400).setScale(2, RoundingMode.HALF_EVEN)));

    }

    @Test
    public void testTransferFailOnDBLock() {
        final String SQL_LOCK_ACC = "SELECT * FROM Account WHERE AccountId = 3 FOR UPDATE";
        Connection conn = null;
        PreparedStatement lockStmt = null;
        ResultSet rs = null;
        Account fromAccount = null;

        try {
            conn = InMemoryDAOFactory.getConnection();
            conn.setAutoCommit(false);
            // lock account for writing:
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC);
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                fromAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
                        rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
            }

            if (fromAccount == null) {
                throw new WebApplicationException("Locking error during test, SQL = " + SQL_LOCK_ACC);
            }
            // after lock account 5, try to transfer from account 6 to 5
            // default h2 timeout for acquire lock is 1sec
            BigDecimal transferAmount = new BigDecimal(50).setScale(2, RoundingMode.HALF_EVEN);

            MoneyTransfer transaction = new MoneyTransfer("GBP", transferAmount, 4L, 3L);
            h2DaoFactory.getAccountDAO().transferAccountBalance(transaction);
            conn.commit();
        } catch (Exception e) {
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
        }

        // now inspect account 4 to verify no transaction occurred
        BigDecimal originalBalance = new BigDecimal(400).setScale(2, RoundingMode.HALF_EVEN);
        assertTrue(h2DaoFactory.getAccountDAO().getAccountById(4).getBalance().equals(originalBalance));
    }

}