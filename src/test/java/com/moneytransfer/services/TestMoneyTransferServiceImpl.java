package com.moneytransfer.services;


import com.moneytransfer.exception.DataNotFoundException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.MoneyTransfer;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertTrue;

/**
 * Integration testing for RestAPI
 * Test data are initialised from src/test/resources/sampleData.sql
 */
public class TestMoneyTransferServiceImpl extends TestService {

    /*
       Scenario: test deposit money to given account number
                 return 200 OK
    */
    @Test
    public void testDeposit() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers/1/deposit/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        Account afterDeposit = mapper.readValue(jsonString, Account.class);
        //check balance is increased from 100 to 200
        assertTrue(afterDeposit.getBalance().equals(new BigDecimal(200).setScale(2, RoundingMode.HALF_EVEN)));

    }

    /*
       Scenario: test deposit money to given non-existing account number
                 return 404 Not Found
    */
    @Test
    public void testDepositOnNonExistingAccount() throws IOException, URISyntaxException, DataNotFoundException {
        URI uri = builder.setPath("/v1/transfers/100/deposit/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 404);
    }

    /*
      Scenario: test withdraw money from account given account number, account has sufficient fund
                return 200 OK
    */
    @Test
    public void testWithDrawSufficientFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers/2/withdraw/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        Account afterDeposit = mapper.readValue(jsonString, Account.class);
        //check balance is decreased from 200 to 100
        assertTrue(afterDeposit.getBalance().equals(new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN)));

    }

    /*
       Scenario: test withdraw money from account given account number, no sufficient fund in account
                 return 403 Forbidden
    */
    @Test
    public void testWithDrawNonSufficientFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers/2/withdraw/1000").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        String responseBody = EntityUtils.toString(response.getEntity());
        assertTrue(statusCode == 403);
    }

    /*
      Scenario: test withdraw money from account given non existing account number
                return 404 Not Found
    */
    @Test
    public void testWithDrawFromNonExistingAccount() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers/200/withdraw/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 404);

    }

    /*
       Scenario: test transaction from one account to another with source account has sufficient fund
                 return 200 OK
    */
    @Test
    public void testTransactionEnoughFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers").build();
        BigDecimal amount = new BigDecimal(10).setScale(2, RoundingMode.HALF_EVEN);
        MoneyTransfer transaction = new MoneyTransfer("USD", amount, 3L, 4L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
    }

    /*
        Scenario: test transaction from one account to another with source account has no sufficient fund
                  return 403 Forbidden
     */
    @Test
    public void testTransactionNotEnoughFund() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers").build();
        BigDecimal amount = new BigDecimal(100000).setScale(2, RoundingMode.HALF_EVEN);
        MoneyTransfer transaction = new MoneyTransfer("USD", amount, 3L, 4L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 403);
    }

    /*
       Scenario: test transaction from one account to another with source/destination account with different currency code
                 return 403 Forbidden
    */
    @Test
    public void testTransactionDifferentCcy() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers").build();
        BigDecimal amount = new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN);
        MoneyTransfer transaction = new MoneyTransfer("EUR", amount, 3L, 4L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 403);

    }

    /*
        Scenario: test transaction from one account to another non existing account
                  return 404 Not Found
     */
    @Test
    public void testTransactionToNonExistingAccount() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/v1/transfers").build();
        BigDecimal amount = new BigDecimal(100000).setScale(2, RoundingMode.HALF_EVEN);
        MoneyTransfer transaction = new MoneyTransfer("USD", amount, 3L, 400L);

        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 404);
    }


}
