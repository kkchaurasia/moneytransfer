--This script is used for unit test cases, DO NOT CHANGE!


DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(50),
Balance DECIMAL(10,2),
CurrencyCode VARCHAR(5)
);


INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('abey',100.00,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('babel',200.00,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('catty',300.00,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('david',400.00,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('eric',500.00,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('frank',600.00,'IND');
