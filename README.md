Problem Statement

Please refer to file title ProblemStatement.pdf

Prerequisite

```
JDK 11
Maven
```

How to run

```
mvn exec:java
```

Requirement

1 You can use Java or Kotlin.

```
Java
```

2 Keep it simple and to the point (e.g. no need to implement any authentication).

```
Service has only one resource `Account` and provide basic functionailty like CURD operation
and moneyTransfer
```

3 Assume the API is invoked by multiple systems and services on behalf of end users.

```
Database locking mechanisms is used during `deposit`, `withdraw`and `money-transfer` operation.
To demostrate concurrency, mutlithreading test are written under file `TestAccountDAO.java`     
```

4 You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 and keep it simple and avoid heavy frameworks.

```
Lightweight library Jersey with embedded Jetty is used to develop RESTful Web Services.
Log4j for logging.
H2 as memory database.
```

5 The datastore should run in-memory for the sake of this test.

```
Using `H2` as In-memory datastore to store application data.
```

6 The final result should be executable as a standalone program (should not require a
pre-installed container/server).

```
No pre-installed container/server requires.
```

7 Demonstrate with tests that the API works as expected.

```
Kindly refer to test under `service` package where test is written at API level
plus also at DAO level.
```

Solution

Please refer to attach swagger file under name `swagger.yml` for API specification and their response's.
You can go to `https://editor.swagger.io/` and paste the content to visualize it.


Account table schema

```
CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    UserName VARCHAR(50),
    Balance DECIMAL(10,2),
    CurrencyCode VARCHAR(5)
    );
```

Sample account entry is already present in database from sql script: `test/resources/sampleData.sql`

`service` package is responsible for declaration of all API routes for account and money-transfer .

`model` package contain all object definition related to application.

`exception` package has user defined Exception and their response mapper.

`config` package is responsible to read runtime application parameter.

`dao` package define initialization and CRUD operations with the underlying storage `H2`.

  
## API  
  1 To create an account
  
  ```
   curl --request POST 'http://localhost:8080/v1/accounts' \
   --header 'Content-Type: application/json' \
   --data-raw '{  
   "userName":"test1",
   "balance":10.0000,
   "currencyCode":"GBP"
   } '
  ```

   2 To get an account by accountID.
   
   ```
   curl --request GET 'http://localhost:8080/v1/accounts/6'
   ```

   3 To get all account exist in our system.
   
   ```
  curl --request GET 'http://localhost:8080/v1/accounts'
   ```
   
   4 To check balance of an account.
   
   ```
  curl  --request GET 'http://localhost:8080/v1/accounts/6/balance'
   ```
   
   5 To deposit money into an account.
   
   ```
  curl  --request PUT 'http://localhost:8080/v1/transfers/6/deposit/10'
   ```
   
   6 To withdraw money from an account.
   
   ```
  curl  --request PUT 'http://localhost:8080/v1/transfers/6/withdraw/10'
   ```
   
   7 To transfer money from one account to other.
   
   ```
  curl  --request POST 'http://localhost:8080/v1/transfers/' \
  --header 'Content-Type: application/json' \
  --data-raw '{  
     "currencyCode":"USD",
     "amount":50.00,
     "fromAccountId":2,
     "toAccountId":1
  }'
   ```